pub const NIGHT_SQUARE_IMAGE: &str = "images/night_square.png";
pub const NIGHT_SQUARE_LABEL: &str = "night_square";
pub const DAY_BALL_IMAGE: &str = "images/day_ball.png";
pub const DAY_BALL_LABEL: &str = "day_ball";
pub const NIGHT_BALL_IMAGE: &str = "images/night_ball.png";
pub const NIGHT_BALL_LABEL: &str = "night_ball";
pub const DAY_SQUARE_IMAGE: &str = "images/day_square.png";
pub const DAY_SQUARE_LABEL: &str = "day_square";

pub const BALL_LAYER: f32 = 5.0;
pub const VISIBLE_FIELD_LAYER: f32 = 3.0;
pub const INVISIBLE_FIELD_LAYER: f32 = 1.0;

// Must be even
pub const FIELD_SIZE: usize = 12;
pub const FIELD_SIZE_HALF: usize = FIELD_SIZE / 2;
// The same size as size of square images (day_square.png and night_square.png)
pub const SQUARE_SIZE: f32 = 50.0;
pub const SQUARE_SIZE_HALF: f32 = SQUARE_SIZE / 2.0;
pub const WINDOW_SIZE: f32 = 50.0;
pub const BALL_SPEED: f32 = 800.0;
// The same as size of ball images
pub const BALL_SIZE: f32 = 40.0;
pub const BALL_SIZE_HALF: f32 = BALL_SIZE / 2.0;
pub const INITIAL_BALL_MOVE: f32 = FIELD_SIZE_HALF as f32 * SQUARE_SIZE_HALF / 2.0;
