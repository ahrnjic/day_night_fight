use crate::constants::{
    BALL_LAYER, DAY_BALL_IMAGE, DAY_BALL_LABEL, DAY_SQUARE_IMAGE, INITIAL_BALL_MOVE,
    INVISIBLE_FIELD_LAYER, NIGHT_BALL_IMAGE, NIGHT_BALL_LABEL, NIGHT_SQUARE_IMAGE,
    VISIBLE_FIELD_LAYER,
};
use crate::game_logic::BallMoveResult;
use crate::game_logic::BallMoveResult::{Change, NoChange};
use crate::game_state::FightSide::{Day, Night};
use crate::game_state::{FightSide, GameState};
use crate::helper::{field_index_to_coordinates, get_square_label};
use rusty_engine::game::{Engine, Game};
use rusty_engine::prelude::Vec2;

pub fn draw_squares(game_state: &GameState, game: &mut Game<GameState>) {
    for (row_index, row) in game_state.field.iter().enumerate() {
        for (square_index, section) in row.iter().enumerate() {
            let (x, y) = field_index_to_coordinates(square_index, row_index);
            let vec2_coordinates = Vec2::new(x, y);

            let day_square_label = get_square_label(Day, row_index, square_index);
            let day_square = game.add_sprite(day_square_label, DAY_SQUARE_IMAGE);

            if *section == Day {
                day_square.layer = VISIBLE_FIELD_LAYER;
            } else {
                day_square.layer = INVISIBLE_FIELD_LAYER;
            }
            day_square.translation = vec2_coordinates;

            let night_square_label = get_square_label(Night, row_index, square_index);

            let night_square = game.add_sprite(night_square_label, NIGHT_SQUARE_IMAGE);

            if *section == Day {
                night_square.layer = INVISIBLE_FIELD_LAYER;
            } else {
                night_square.layer = VISIBLE_FIELD_LAYER;
            }

            night_square.translation = vec2_coordinates;
        }
    }
}

// Day-looking ball if fighting for night and night-looking ball is fighting for day.
// That's the reason why day-looking ball is called night ball and vice versa in the code
pub fn draw_balls(game: &mut Game<GameState>) {
    let day_ball = game.add_sprite(DAY_BALL_LABEL, NIGHT_BALL_IMAGE);
    day_ball.translation = Vec2::new(-INITIAL_BALL_MOVE, INITIAL_BALL_MOVE);
    day_ball.layer = BALL_LAYER;

    let night_ball = game.add_sprite(NIGHT_BALL_LABEL, DAY_BALL_IMAGE);
    night_ball.translation = Vec2::new(INITIAL_BALL_MOVE, INITIAL_BALL_MOVE);
    night_ball.layer = BALL_LAYER;
}

pub fn change_square_visibility(engine: &mut Engine, ball_move_result: BallMoveResult) {
    match ball_move_result {
        NoChange => return,
        Change(x, y, fight_side) => {
            let day_square_label = get_square_label(Day, x, y);
            let night_square_label = get_square_label(Night, x, y);

            let day_square = engine.sprites.get_mut(&day_square_label).unwrap();
            day_square.layer = get_square_visibility_layer(Day, fight_side);

            let night_square = engine.sprites.get_mut(&night_square_label).unwrap();
            night_square.layer = get_square_visibility_layer(Night, fight_side);
        }
    }
}

// Private
fn get_square_visibility_layer(
    square_fight_side: FightSide,
    ball_fight_side_result: FightSide,
) -> f32 {
    if square_fight_side == ball_fight_side_result {
        VISIBLE_FIELD_LAYER
    } else {
        INVISIBLE_FIELD_LAYER
    }
}
