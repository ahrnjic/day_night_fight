use crate::constants::{
    BALL_SIZE_HALF, BALL_SPEED, DAY_BALL_LABEL, FIELD_SIZE_HALF, NIGHT_BALL_LABEL, SQUARE_SIZE,
};
use crate::draw::change_square_visibility;
use crate::game_logic::BallMoveResult::{Change, NoChange};
use crate::game_state::BallDirection::{DownLeft, DownRight, UpLeft, UpRight};
use crate::game_state::FightSide::{Day, Night};
use crate::game_state::{BallDirection, FightSide, GameState};
use crate::helper::EdgeSide::{Down, Left, Right, Up};
use crate::helper::{coordinates_to_field_index, get_ball_edge_coordinate, EdgeSide};
use rusty_engine::game::Engine;
use rusty_engine::prelude::{Sprite, Vec2};

#[derive(PartialEq)]
pub enum BallMoveResult {
    NoChange,
    Change(usize, usize, FightSide),
}

pub fn game_logic(engine: &mut Engine, game_state: &mut GameState) {
    move_ball(Night, engine, game_state);
    move_ball(Day, engine, game_state);
}

// Private
fn move_ball(ball_fight_side: FightSide, engine: &mut Engine, game_state: &mut GameState) {
    let (ball_label, ball_direction) = get_ball_info(ball_fight_side, game_state);

    let ball = engine.sprites.get_mut(ball_label).unwrap();

    let ball_move_result = match ball_direction {
        UpRight => move_up_right(ball, ball_fight_side, game_state, engine.delta_f32),
        UpLeft => move_up_left(ball, ball_fight_side, game_state, engine.delta_f32),
        DownRight => move_down_right(ball, ball_fight_side, game_state, engine.delta_f32),
        DownLeft => move_down_left(ball, ball_fight_side, game_state, engine.delta_f32),
    };

    change_square_visibility(engine, ball_move_result);
}

fn move_up_right(
    ball: &mut Sprite,
    ball_fight_side: FightSide,
    game_state: &mut GameState,
    delta: f32,
) -> BallMoveResult {
    ball.translation.x += BALL_SPEED * delta;
    ball.translation.y += BALL_SPEED * delta;

    if border_is_reached(&(ball.translation), Right) {
        game_state.change_ball_direction(ball_fight_side, UpLeft);
        return NoChange;
    }

    if border_is_reached(&(ball.translation), Up) {
        game_state.change_ball_direction(ball_fight_side, DownRight);
        return NoChange;
    }

    let (x, y) =
        coordinates_to_field_index(ball.translation.x + BALL_SIZE_HALF, ball.translation.y);

    if game_state.field[x][y] != ball_fight_side {
        game_state.change_ball_direction(ball_fight_side, UpLeft);
        game_state.change_square_fight_side(x, y, ball_fight_side);
        return Change(x, y, ball_fight_side);
    }

    let (x, y) =
        coordinates_to_field_index(ball.translation.x, ball.translation.y + BALL_SIZE_HALF);
    if game_state.field[x][y] != ball_fight_side {
        game_state.change_ball_direction(ball_fight_side, DownRight);
        game_state.change_square_fight_side(x, y, ball_fight_side);
        return Change(x, y, ball_fight_side);
    }

    return NoChange;
}

fn move_up_left(
    ball: &mut Sprite,
    ball_fight_side: FightSide,
    game_state: &mut GameState,
    delta: f32,
) -> BallMoveResult {
    ball.translation.x -= BALL_SPEED * delta;
    ball.translation.y += BALL_SPEED * delta;

    if border_is_reached(&(ball.translation), Left) {
        game_state.change_ball_direction(ball_fight_side, UpRight);
        return NoChange;
    }

    if border_is_reached(&(ball.translation), Up) {
        game_state.change_ball_direction(ball_fight_side, DownLeft);
        return NoChange;
    }

    let (x, y) =
        coordinates_to_field_index(ball.translation.x - BALL_SIZE_HALF, ball.translation.y);
    if game_state.field[x][y] != ball_fight_side {
        game_state.change_ball_direction(ball_fight_side, UpRight);
        game_state.change_square_fight_side(x, y, ball_fight_side);
        return Change(x, y, ball_fight_side);
    }

    let (x, y) =
        coordinates_to_field_index(ball.translation.x, ball.translation.y + BALL_SIZE_HALF);
    if game_state.field[x][y] != ball_fight_side {
        game_state.change_ball_direction(ball_fight_side, DownLeft);
        game_state.change_square_fight_side(x, y, ball_fight_side);
        return Change(x, y, ball_fight_side);
    }

    return NoChange;
}

fn move_down_left(
    ball: &mut Sprite,
    ball_fight_side: FightSide,
    game_state: &mut GameState,
    delta: f32,
) -> BallMoveResult {
    ball.translation.x -= BALL_SPEED * delta;
    ball.translation.y -= BALL_SPEED * delta;

    if border_is_reached(&(ball.translation), Left) {
        game_state.change_ball_direction(ball_fight_side, DownRight);
        return NoChange;
    }

    if border_is_reached(&(ball.translation), Down) {
        game_state.change_ball_direction(ball_fight_side, UpLeft);
        return NoChange;
    }

    let (x, y) =
        coordinates_to_field_index(ball.translation.x - BALL_SIZE_HALF, ball.translation.y);
    if game_state.field[x][y] != ball_fight_side {
        game_state.change_ball_direction(ball_fight_side, DownRight);
        game_state.change_square_fight_side(x, y, ball_fight_side);
        return Change(x, y, ball_fight_side);
    }

    let (x, y) =
        coordinates_to_field_index(ball.translation.x, ball.translation.y - BALL_SIZE_HALF);
    if game_state.field[x][y] != ball_fight_side {
        game_state.change_ball_direction(ball_fight_side, UpLeft);
        game_state.change_square_fight_side(x, y, ball_fight_side);
        return Change(x, y, ball_fight_side);
    }

    return NoChange;
}

fn move_down_right(
    ball: &mut Sprite,
    ball_fight_side: FightSide,
    game_state: &mut GameState,
    delta: f32,
) -> BallMoveResult {
    ball.translation.x += BALL_SPEED * delta;
    ball.translation.y -= BALL_SPEED * delta;

    if border_is_reached(&(ball.translation), Right) {
        game_state.change_ball_direction(ball_fight_side, DownLeft);
        return NoChange;
    }

    if border_is_reached(&(ball.translation), Down) {
        game_state.change_ball_direction(ball_fight_side, UpRight);
        return NoChange;
    }

    let (x, y) =
        coordinates_to_field_index(ball.translation.x + BALL_SIZE_HALF, ball.translation.y);
    if game_state.field[x][y] != ball_fight_side {
        game_state.change_ball_direction(ball_fight_side, DownLeft);
        game_state.change_square_fight_side(x, y, ball_fight_side);
        return Change(x, y, ball_fight_side);
    }

    let (x, y) =
        coordinates_to_field_index(ball.translation.x, ball.translation.y - BALL_SIZE_HALF);
    if game_state.field[x][y] != ball_fight_side {
        game_state.change_ball_direction(ball_fight_side, UpRight);
        game_state.change_square_fight_side(x, y, ball_fight_side);
        return Change(x, y, ball_fight_side);
    }

    return NoChange;
}

fn border_is_reached(ball_coordinates: &Vec2, edge_side: EdgeSide) -> bool {
    let coordinate = get_ball_edge_coordinate(ball_coordinates, &edge_side);

    match edge_side {
        Right | Up => coordinate >= SQUARE_SIZE * FIELD_SIZE_HALF as f32,
        Left | Down => coordinate <= -SQUARE_SIZE * FIELD_SIZE_HALF as f32,
    }
}

fn get_ball_info(fight_side: FightSide, game_state: &mut GameState) -> (&str, &BallDirection) {
    if fight_side == Day {
        (DAY_BALL_LABEL, &game_state.day_ball_direction)
    } else {
        (NIGHT_BALL_LABEL, &game_state.night_ball_direction)
    }
}
