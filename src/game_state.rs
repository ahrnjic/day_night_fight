use crate::constants::{FIELD_SIZE, FIELD_SIZE_HALF};
use crate::game_state::BallDirection::{DownRight, UpLeft};
use crate::game_state::FightSide::{Day, Night};
use rusty_engine::prelude::Resource;

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum FightSide {
    Day,
    Night,
}

pub enum BallDirection {
    UpRight,
    UpLeft,
    DownRight,
    DownLeft,
}

pub struct GameState {
    pub field: [[FightSide; FIELD_SIZE]; FIELD_SIZE],
    pub day_ball_direction: BallDirection,
    pub night_ball_direction: BallDirection,
}

impl GameState {
    pub fn change_ball_direction(&mut self, fight_side: FightSide, ball_direction: BallDirection) {
        match fight_side {
            Day => {
                self.day_ball_direction = ball_direction;
            }
            Night => {
                self.night_ball_direction = ball_direction;
            }
        }
    }
    pub fn change_square_fight_side(&mut self, x: usize, y: usize, fight_side: FightSide) {
        self.field[x][y] = fight_side;
    }
}

impl Resource for GameState {}

impl Default for GameState {
    fn default() -> Self {
        Self {
            field: [[[Day; FIELD_SIZE_HALF], [Night; FIELD_SIZE_HALF]]
                .concat()
                .as_slice()
                .try_into()
                .unwrap(); FIELD_SIZE],
            day_ball_direction: DownRight,
            night_ball_direction: UpLeft,
        }
    }
}
