use crate::constants::{
    BALL_SIZE_HALF, DAY_SQUARE_LABEL, FIELD_SIZE_HALF, NIGHT_SQUARE_LABEL, SQUARE_SIZE,
    SQUARE_SIZE_HALF,
};
use crate::game_state::FightSide;
use crate::helper::EdgeSide::{Down, Left, Right, Up};
use rusty_engine::prelude::Vec2;

pub fn field_index_to_coordinates(x: usize, y: usize) -> (f32, f32) {
    let mut coordinates = (0.0, 0.0);

    if x < FIELD_SIZE_HALF {
        coordinates.0 = -((FIELD_SIZE_HALF - x - 1) as f32 * SQUARE_SIZE) - SQUARE_SIZE_HALF;
    } else {
        coordinates.0 = (x + 1 - FIELD_SIZE_HALF) as f32 * SQUARE_SIZE - SQUARE_SIZE_HALF;
    }

    if y < FIELD_SIZE_HALF {
        coordinates.1 = -((FIELD_SIZE_HALF - y - 1) as f32 * SQUARE_SIZE) - SQUARE_SIZE_HALF;
    } else {
        coordinates.1 = (y + 1 - FIELD_SIZE_HALF) as f32 * SQUARE_SIZE - SQUARE_SIZE_HALF;
    }

    return coordinates;
}

pub fn coordinates_to_field_index(x: f32, y: f32) -> (usize, usize) {
    let mut field_index = (0, 0);

    if x < 0_f32 {
        field_index.1 = FIELD_SIZE_HALF - 1 - (-x / SQUARE_SIZE) as usize
    } else {
        field_index.1 = FIELD_SIZE_HALF + (x / SQUARE_SIZE) as usize
    }

    if y < 0_f32 {
        field_index.0 = FIELD_SIZE_HALF - 1 - (-y / SQUARE_SIZE) as usize
    } else {
        field_index.0 = FIELD_SIZE_HALF + (y / SQUARE_SIZE) as usize
    }

    return field_index;
}

pub fn get_square_label(fight_side: FightSide, x: usize, y: usize) -> String {
    let prefix = match fight_side {
        FightSide::Day => DAY_SQUARE_LABEL,
        FightSide::Night => NIGHT_SQUARE_LABEL,
    };

    return prefix.to_string() + x.to_string().as_str() + "_" + y.to_string().as_str();
}

pub enum EdgeSide {
    Right,
    Left,
    Down,
    Up,
}

pub fn get_ball_edge_coordinate(ball_coordinates: &Vec2, edge_side: &EdgeSide) -> f32 {
    match edge_side {
        Right => ball_coordinates.x + BALL_SIZE_HALF,
        Left => ball_coordinates.x - BALL_SIZE_HALF,
        Down => ball_coordinates.y - BALL_SIZE_HALF,
        Up => ball_coordinates.y + BALL_SIZE_HALF,
    }
}
