mod constants;
mod draw;
mod game_logic;
mod game_state;
mod helper;
mod settings;

use crate::draw::{draw_balls, draw_squares};
use crate::game_logic::game_logic;
use crate::game_state::GameState;
use crate::settings::window_settings;
use rusty_engine::game::Game;

// An infinite fight between day and night
// Choose your hero, or be quiet!
fn main() {
    let mut game: Game<GameState> = Game::new();
    let game_state = GameState::default();

    window_settings(&mut game);

    draw_balls(&mut game);
    draw_squares(&game_state, &mut game);

    game.add_logic(game_logic);
    game.run(game_state);
}
