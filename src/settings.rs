use crate::game_state::GameState;
use rusty_engine::game::WindowMode::Fullscreen;
use rusty_engine::game::{Game, Window};

pub fn window_settings(game: &mut Game<GameState>) {
    game.window_settings(Window {
        mode: Fullscreen,
        ..Window::default()
    });
}
